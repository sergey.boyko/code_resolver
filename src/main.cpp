//
// Created by bso on 11.09.17.
//
#include <iostream>
#include <unistd.h>
#include "json.h"
#include "config_parser.h"
#include "analysis/Resolver.h"
#include "ProgramLog.h"
#include "precedence_method/PrecedenceFunctions.h"

int main(int argc, char *argv[]) {
	if(argc < 3) {
		std::cerr << "invalid config path" << std::endl;
		return 1;
	}

	std::string config_path = argv[1];
	config_parser parser;
	if(!parser.parse(config_path)) {
		std::cerr << "couldn't parse config: " << config_path << std::endl;
		return 1;
	}

	ProgramLog::LogInit("code-resolver", parser.log_path, true, getpid(),
						parser.clear_log);

	//laba 1
	PrecedenceFunctions functions(parser.precedence_matrix);
	std::pair<std::vector<int>, std::vector<int>> result;
	if(!functions.Build(result)) {
		LOG_I() << "Can not be built precedence functions";
	} else {
		std::stringstream ss_f, ss_g;

		ss_f << "f(s): ";
		for(auto it : result.first) {
			ss_f << it << " ";
		}
		LOG_I() << ss_f.str();

		ss_g << "g(s): ";
		for(auto it : result.second) {
			ss_g << it << " ";
		}
		LOG_I() << ss_g.str();
	}

	//laba 1
	std::stringstream ss;
	for(auto it:parser.input_line){
	    ss << it;
	}
	LOG_I() << "Input line = " << ss.str();

	Resolver rs(parser);
	std::string result;
	if (!rs.resolve(result)){
	    LOG_I() << "Input line does not belongs language";
	    return 0;
	}

	LOG_I() << "Input line belongs language";
	LOG_I() << result;

	LOG_D() << '/n';

	return 0;
}