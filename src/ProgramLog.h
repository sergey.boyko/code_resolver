//
// Created by bso on 22.09.17.
//

#ifndef ALPMAIN_PROGRAMLOG_H
#define ALPMAIN_PROGRAMLOG_H

#include <fstream>
#include <map>
#include <memory>
#include <iostream>

enum LogType{
    t_debug = 1,
    t_error = 2,
    t_info = 3
};

class ProgramLog{
public:
    /**
     * Get instance
     * @return - this
     */
    static ProgramLog *GetInstance();

    /**
     * Log initialization
     * @param module - module name
     * @param path - path to log directory
     * @param is_console_allowed - true, if application is allowed to use console
     * @param pid - process id application
     * @param clear_logs - true, if need to clear log files
     * @param role - role name
     */
    static void LogInit(const std::string &module,
                        const std::string &path,
                        bool is_console_allowed,
                        __pid_t pid,
                        bool clear_logs,
                        const std::string &role = "");

    /**
     * Start Typing
     * @param type - log type
     * @param file - cpp file from which the function was called
     * @return - this
     */
	ProgramLog &StartTyping(LogType type, const std::string &file, int line);

    /**
     * Stream write
     * @tparam T - type message
     * @param message - message
     * @return - this
     */
    template<typename T>
    ProgramLog &operator<<(const T &message){
        std::ofstream &fstream = preperationToWrite(false);
        fstream << message;
        fstream.close();

        if (m_is_console_allowed){
            switch (m_used_log_type){
                case t_debug:{
                    std::cout << message;
                    break;
                }
                case t_error:{
                    std::cerr << message;
                    break;
                }
                default:{
                    std::cout << message;
                    break;
                }
            }
        }

        return *this;
    }

protected:
    /**
     * Instance
     */
    static std::shared_ptr<ProgramLog> Instance;

    /**
     * Ctor
     * @param module - module name
     * @param path - path to log directory
     * @param is_console_allowed - true, if application is allowed to use console
     * @param pid - process id application
     * @param clear_logs - true, if need to clear log files
     * @param role - role name
     */
    ProgramLog(const std::string &module, const std::string &path, bool is_console_allowed, __pid_t pid,
               bool clear_logs,
               const std::string &role = ""):
            m_module_name(module),
            m_path(path),
            m_is_console_allowed(is_console_allowed),
            m_pid(pid),
            m_role(role),
            m_clear_debug_log(clear_logs),
            m_clear_error_log(clear_logs),
            m_clear_info_log(clear_logs){
    }

    /**
     * Get today date
     * @return - string
     */
    std::string today_date();

    /**
     * Get time from now and file from which log typing was called
     * @param file - file from which log typing was called
     * @return - string
     */
	std::string timeNowAndFileSource(const std::string &file, int line);

    /**
     * Preperation to write
     * @param clear_logs - true, if need to clear log files
     * @return - ofstream
     */
    std::ofstream &preperationToWrite(bool clear_logs);

    /**
     * module name
     */
    std::string m_module_name = "";

    /**
     * role name
     */
    std::string m_role = "";

    /**
     * Is application allowed to use console
     */
    bool m_is_console_allowed;

    /**
     * Is need to clear debug log
     */
    bool m_clear_debug_log;

    /**
     * Is need to clear error log
     */
    bool m_clear_error_log;

    /**
     * Is need to clear info log
     */
    bool m_clear_info_log;

    /**
     * process id application
     */
    __pid_t m_pid = 0;

    /**
     * path to log directory
     */
    std::string m_path;

    /**
     * debug file stream
     */
    std::ofstream m_fdebug;

    /**
     * error file stream
     */
    std::ofstream m_ferror;

    /**
     * info file stream
     */
    std::ofstream m_finfo;

    /**
     * log type that is currently using the application
     */
    LogType m_used_log_type;

};


#define LOG_D() ProgramLog::GetInstance()->StartTyping(t_debug,__FILE__,__LINE__)
#define LOG_E() ProgramLog::GetInstance()->StartTyping(t_error,__FILE__,__LINE__)
#define LOG_I() ProgramLog::GetInstance()->StartTyping(t_info,__FILE__,__LINE__)

#endif //ALPMAIN_PROGRAMLOG_H
