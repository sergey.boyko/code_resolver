﻿#pragma once

#include <string>
#include <vector>
#include "json.h"
#include "analysis/rule_info.h"

enum RatioType {
	Empty = 0,
	Equal = 1,
	More = 2,
	Less = 3
};

class config_parser {
public:
	config_parser() = default;

	bool parse(const std::string &config_path);

	std::vector<rule_info> rules;
	std::vector<char> terminals;
	std::vector<char> input_line;

	//lab 2
	std::vector<std::vector<int>> precedence_matrix;

	std::string log_path;
	bool clear_log;

private:
	void erase_spaces_from_input_line();

	std::string address_config_file;
};