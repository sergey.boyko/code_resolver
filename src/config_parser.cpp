﻿#include "config_parser.h"
#include "analysis/rule_info.h"
#include <fstream>
#include <iostream>

bool config_parser::parse(const std::string &config_path) {
	address_config_file = config_path;
	Json::Value root;
	Json::Reader reader;
	std::ifstream in(address_config_file);

	if(!reader.parse(in, root)) {
		std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": Could't parse configuration file" << std::endl;
		return false;
	}

	try {
		auto _inline = root["input_line"].asString();
		for(const auto &it:_inline) {
			input_line.push_back(it);
		}

		for(const auto &it:root["lang"]) {
			////rule always is one symbol char
			rules.emplace_back(it["rule"].asString()[0],
							   it["alternative"].asString(),
							   it["number_alt"].asInt());
		}

		for(const auto &it:root["terminals"]) {
			////rule always is one symbol char
			terminals.push_back(it.asString()[0]);
		}

		log_path = root["log-path"].asString();
		clear_log = root["clear-log"].asBool();

		erase_spaces_from_input_line();

		//lab 2
		auto row_num = root["precedence-matrix"]["row-num"].asInt();
		auto column_num = root["precedence-matrix"]["column-num"].asInt();
		precedence_matrix.resize(row_num, std::vector<int>(column_num,0));

		for(const auto &it:root["precedence-matrix"]["ratios"]) {
			auto pair_names = it.getMemberNames()[0];
			if(pair_names.size() != 4) {
				return false;
			}
			if(pair_names[0] == 'F' && pair_names[2] == 'G') {
				auto row = atoi(&pair_names[1]) - 1;
				auto column = atoi(&pair_names[3]) - 1;
				auto key = it[pair_names].asUInt();
				precedence_matrix[row][column] = key;
			}
		}
	}
	catch(const Json::LogicError &er) {
		std::cerr << er.what() << std::endl;
		return false;
	}
	catch(const std::exception &ec) {
		std::cerr << __PRETTY_FUNCTION__ << ":" << __LINE__ << ": Incorrect field values in the configuration file: "
				  << ec.what() << std::endl;
		return false;
	}

	if(rules.empty()) {
		std::cerr << "empty lang" << std::endl;
		return false;
	}

	return true;
}

void config_parser::erase_spaces_from_input_line() {
	for(auto i = 0; i < input_line.size(); ++i) {
		if(input_line[i] == ' ') {
			input_line.erase(input_line.begin() + i);
		}
	}
}
