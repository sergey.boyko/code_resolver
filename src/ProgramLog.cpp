//
// Created by bso on 22.09.17.
//

#include <sstream>
#include "ProgramLog.h"
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <string>
#include <iostream>
#include <sys/time.h>

std::shared_ptr<ProgramLog> ProgramLog::Instance;

ProgramLog *ProgramLog::GetInstance() {
	if(!Instance) {
		throw std::runtime_error("Instance has not been created yet");
	}

	return Instance.get();
}

std::string ProgramLog::today_date() {
	std::time_t curent_time = time(0);
	struct tm tstruct;
	tstruct = *localtime(&curent_time);

	std::string month = std::to_string(tstruct.tm_mon + 1);
	std::string day = std::to_string(tstruct.tm_mday);
	std::string year = std::to_string(tstruct.tm_year + 1900);

	if(stoi(day) < 10) {
		day = "0" + day;
	}

	return year + "-" + month + "-" + day;
}

std::string ProgramLog::timeNowAndFileSource(const std::string &file, int line) {
	std::stringstream time_stream;
	std::time_t curent_time = time(0);
	struct tm tstruct;
	tstruct = *localtime(&curent_time);

	time_stream << tstruct.tm_hour << ":" << tstruct.tm_min << ":" << tstruct.tm_sec;
	auto time_from_now = clock();
	std::string time = "[";

	time += time_stream.str() + ":" + std::to_string(time_from_now) + "]: " + file + ":" + std::to_string(line) + ": ";
	return time;
}

void
ProgramLog::LogInit(const std::string &module, const std::string &path, bool is_console_allowed, __pid_t pid,
					bool clear_logs,
					const std::string &role) {
	std::ios::sync_with_stdio(false);
	Instance.reset(new ProgramLog(module, path, is_console_allowed, pid, clear_logs, role));
}

ProgramLog &ProgramLog::StartTyping(LogType type, const std::string &file, int line) {
	m_used_log_type = type;
	bool clear_log = false;
	switch(type) {
		case t_debug: {
			clear_log = m_clear_debug_log;
			m_clear_debug_log = false;
			break;
		}
		case t_error: {
			clear_log = m_clear_error_log;
			m_clear_error_log = false;
			break;
		}
		case t_info: {
			clear_log = m_clear_info_log;
			m_clear_info_log = false;
			break;
		}
	}

	std::ofstream &fstream = preperationToWrite(clear_log);

	fstream << std::endl << timeNowAndFileSource(file, line);
	if(m_is_console_allowed) {
		switch(type) {
			case t_debug: {
				std::cout << std::endl << timeNowAndFileSource(file, line);
				break;
			}
			case t_error: {
				std::cerr << std::endl << timeNowAndFileSource(file, line);
				break;
			}
			default: {
				std::cout << std::endl << timeNowAndFileSource(file, line);
				break;
			}
		}
	}
	fstream.close();

	return *this;
}

std::ofstream &ProgramLog::preperationToWrite(bool clear_log) {
	auto mod = std::ios::app;
	if(clear_log) {
		mod = std::ios::out;
	}

	std::string lname;

	if(!m_role.empty()) {
		lname = m_role + "." + std::to_string(m_pid) + ".";
	}
	lname += "log";

	try {
		switch(m_used_log_type) {
			case t_debug: {
				m_fdebug.open(m_path + m_module_name + ProgramLog::today_date() + ".debug." + lname, mod);
				return m_fdebug;
			}
			case t_error: {
				m_ferror.open(m_path + m_module_name + ProgramLog::today_date() + ".error." + lname, mod);
				return m_ferror;
			}
			case t_info: {
				m_finfo.open(m_path + m_module_name + ProgramLog::today_date() + ".info." + lname, mod);
				return m_finfo;
			}
			default: {
				throw std::runtime_error("Empty m_used_log_type");
			}
		}
	}
	catch(const std::ios::failure &ec) {
		throw std::runtime_error(
				"Couldn't open file:" + m_path + m_module_name + ProgramLog::today_date() + ".type." + lname);
	}
}