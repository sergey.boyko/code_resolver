//
// Created by bso on 09.01.18.
//

#ifndef TRANSLATOR_PRECEDENCENODE_H
#define TRANSLATOR_PRECEDENCENODE_H


#include <memory>
#include <algorithm>
#include <vector>
#include <sstream>
#include "../ProgramLog.h"

struct Node: public std::enable_shared_from_this<Node> {
	Node() = default;

	Node(const Node &node):
			m_parent(node.m_parent),
			m_childs(m_childs) {
	}

	Node(const std::vector<std::string> &key):
			m_key(key) {
	}

	void AddChild(const std::shared_ptr<Node> &child) {
		auto it = std::find(m_childs.begin(), m_childs.end(), child);
		if(it != m_childs.end()) {
			return;
		}

		m_childs.push_back(child);
		child->m_parent.push_back(shared_from_this());
	}

	void PrintThirdParty() {
		std::string node_name;
		for(const auto &name : m_key) {
			node_name += name;
		}

		std::stringstream child_stream;
		for(const auto &child : m_childs) {
			std::string child_name;
			for(const auto &name : child->m_key) {
				child_name += name;
			}
			child_stream << child_name << ", ";
		}
		LOG_D() << node_name + "->: " << child_stream.str();

		std::stringstream parent_stream;
		for(const auto &parent : m_parent) {
			std::string parent_name;
			for(const auto &name : parent->m_key) {
				parent_name += name;
			}
			parent_stream << parent_name << ", ";
		}
		LOG_D() << node_name + "<-: " << parent_stream.str();
	}

	std::string GetName() {
		std::string name;
		for(const auto &key : m_key) {
			name += key;
		}
		return name;
	}

	std::vector<std::shared_ptr<Node>> m_childs;
	std::vector<std::shared_ptr<Node>> m_parent;

	std::vector<std::string> m_key;
};


#endif //TRANSLATOR_PRECEDENCENODE_H
