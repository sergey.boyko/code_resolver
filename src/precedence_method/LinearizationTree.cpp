//
// Created by bso on 08.01.18.
//

#include <algorithm>
#include "LinearizationTree.h"
#include "../ProgramLog.h"
#include "../config_parser.h"

LinearizationTree::LinearizationTree(const std::vector<std::vector<int>> &precedence_matrix) {
	for(auto i = 0; i < precedence_matrix.size(); ++i) {
		m_nodes.push_back(std::make_shared<Node>(
				std::vector<std::string>(1, "F" + std::to_string(i + 1))));
		m_nodes.push_back(std::make_shared<Node>(
				std::vector<std::string>(1, "G" + std::to_string(i + 1))));
	}

	for(auto i = 0; i < precedence_matrix.size(); ++i) {
		for(auto j = 0; j < precedence_matrix[i].size(); ++j) {
			if(precedence_matrix[i][j] <= Equal) {
				continue;
			}

			if(precedence_matrix[i][j] == More) {
				//FIXME
				auto parent = FindNode(std::string("F") + std::to_string(i + 1));
				auto child = FindNode(std::string("G") + std::to_string(j + 1));
				//m_nodes[i]->AddChild(m_nodes[precedence_matrix.size() + j]);
				parent->get()->AddChild(*child);
			} else {
				//precedence_matrix[i][j] == Less
				auto parent = FindNode(std::string("G") + std::to_string(j + 1));
				auto child = FindNode(std::string("F") + std::to_string(i + 1));
				//m_nodes[precedence_matrix.size() + j]->AddChild(m_nodes[i]);
				parent->get()->AddChild(*child);
			}
		}
	}

	//PrintTree();
	for(auto i = 0; i < precedence_matrix.size(); ++i) {
		auto name = std::string("F") + std::to_string(i + 1);
		auto node1 = FindNode(name, false);

		std::vector<std::shared_ptr<Node>> merge_nodes;
		merge_nodes.push_back(*node1);
		for(auto j = 0; j < precedence_matrix[i].size(); ++j) {
			if(precedence_matrix[i][j] == 1) {
				auto node2 = FindNode("G" + std::to_string(j + 1), false);
				merge_nodes.push_back(*node2);
			}
		}

		if(merge_nodes.size() != 1) {
			mergeNodes(merge_nodes);
		}
	}
	//RemoveSimpleNodes();
}

void LinearizationTree::RemoveNode(const std::shared_ptr<Node> &node) {
	for(auto &child : node->m_childs) {
		auto it = std::find(child->m_parent.begin(),
							child->m_parent.end(),
							node);
		if(it == child->m_parent.end()) {
			throw std::runtime_error("Child has not parent");
		}
		child->m_parent.erase(it);
	}

	for(auto &parent : node->m_parent) {
		auto it = std::find(parent->m_childs.begin(),
							parent->m_childs.end(),
							node);
		if(it == parent->m_childs.end()) {
			throw std::runtime_error("Parent has not child");
		}
		parent->m_childs.erase(it);
	}

	resetNode(node);
}

void LinearizationTree::resetNode(const std::shared_ptr<Node> &node) {
	auto it = std::find(m_nodes.begin(), m_nodes.end(), node);
	if(it == m_nodes.end()) {
		throw std::runtime_error("Couldn't find node");
	}

	m_nodes.erase(it);
}

void LinearizationTree::mergeNodes(const std::vector<std::shared_ptr<Node>> &nodes) {
	std::vector<std::string> final_node_name;
	for(const auto &node : nodes) {
		for(const auto &node_name : node->m_key) {
			final_node_name.push_back(node_name);
		}
	}

	std::shared_ptr<Node> final_node(new Node(final_node_name));
	for(const auto &node: nodes) {
		for(const auto &node_child : node->m_childs) {
			final_node->AddChild(node_child);
		}

		for(const auto &node_parent : node->m_parent) {
			node_parent->AddChild(final_node);
		}
	}

	m_nodes.push_back(final_node);
	for(auto &node : nodes) {
		RemoveNode(node);
	}
}

std::vector<std::shared_ptr<Node>>::iterator
LinearizationTree::FindNode(const std::string &node_name, bool canNotFound) {
	auto node = std::find_if(m_nodes.begin(),
							 m_nodes.end(),
							 [&](const std::shared_ptr<Node> &ifnode) {
								 return std::find(ifnode->m_key.begin(),
												  ifnode->m_key.end(),
												  node_name) != ifnode->m_key.end();
							 });
	if(node == m_nodes.end() && !canNotFound) {
		LOG_E() << "Couldn't find node";
		throw std::runtime_error("Couldn't find node");
	}
	return node;
}

void LinearizationTree::RemoveSimpleNodes() {
	for(const auto &node:m_nodes) {
		if(node->m_parent.empty() && node->m_childs.empty()) {
			resetNode(node);
			RemoveSimpleNodes();
			return;
		}
	}
}

void LinearizationTree::PrintTree() {
	std::stringstream ss;
	for(const auto &node : m_nodes) {
		ss << node->GetName() << ", ";
	}
	LOG_D() << "PrintTree() " << ss.str();

	for(const auto &node : m_nodes) {
		node->PrintThirdParty();
		LOG_D() << '\n';
	}
}