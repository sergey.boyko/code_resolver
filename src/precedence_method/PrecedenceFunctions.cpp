//
// Created by bso on 08.01.18.
//

#include "PrecedenceFunctions.h"

PrecedenceFunctions::PrecedenceFunctions(std::vector<std::vector<int>> &matrix):
		m_precedence_matrix(matrix),
		m_tree(matrix) {
	m_tree.PrintTree();
}

bool PrecedenceFunctions::Build(std::pair<std::vector<int>, std::vector<int>> &result) {
	if(!checkExistence()) {
		return false;
	}

	std::vector<int> precedenceFunctions_F;
	std::vector<int> precedenceFunctions_G;

	for(auto i = 0; i < m_precedence_matrix.size(); ++i) {
		auto node = m_tree.FindNode("F" + std::to_string(i + 1), true);
		if(node != m_tree.m_nodes.end()) {
			precedenceFunctions_F.push_back(lengthLongestPath(*node));
		}
	}

	for(auto i = 0; i < m_precedence_matrix.size(); ++i) {
		auto node = m_tree.FindNode("G" + std::to_string(i + 1), true);
		if(node != m_tree.m_nodes.end()) {
			precedenceFunctions_G.push_back(lengthLongestPath(*node));
		}
	}

	result.first = precedenceFunctions_F;
	result.second = precedenceFunctions_G;
	return true;
}

bool PrecedenceFunctions::checkExistence() {
	LOG_D() << "checkExistence()";
	//Get m_tree copy
	LinearizationTree tree(m_precedence_matrix);
	tree.RemoveSimpleNodes();

	while(!tree.m_nodes.empty()) {
		auto isRemove = false;
		for(auto &node : tree.m_nodes) {
			if(node->m_childs.empty()) {
				LOG_D() << "Tree after remove " << node->GetName();
				tree.RemoveNode(node);
				tree.PrintTree();
				isRemove = true;
				break;
			}
		}

		if(!isRemove) {
			return false;
		}
		tree.RemoveSimpleNodes();
	}

	//tree is empty
	return true;
}

unsigned int PrecedenceFunctions::lengthLongestPath(const std::shared_ptr<Node> &node) {
	unsigned int max = 0;
	for(auto child : node->m_childs) {
		auto length = lengthLongestPath(child) + 1;
		if(length > max) {
			max = length;
		}
	}

	return max;
}
