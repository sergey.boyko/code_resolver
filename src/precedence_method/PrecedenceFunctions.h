//
// Created by bso on 08.01.18.
//

#ifndef TRANSLATOR_PRECEDENCEFUNCTION_H
#define TRANSLATOR_PRECEDENCEFUNCTION_H


#include <vector>
#include "../config_parser.h"
#include "LinearizationTree.h"

class PrecedenceFunctions {
public:
	PrecedenceFunctions(std::vector<std::vector<int>> &matrix);

	bool Build(std::pair<std::vector<int>, std::vector<int>> &result);

private:
	bool checkExistence();

	unsigned int lengthLongestPath(const std::shared_ptr<Node> &node);

	std::vector<std::vector<int>> m_precedence_matrix;
	LinearizationTree m_tree;
};


#endif //TRANSLATOR_PRECEDENCEFUNCTION_H
