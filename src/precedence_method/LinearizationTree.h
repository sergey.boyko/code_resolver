//
// Created by bso on 08.01.18.
//

#ifndef TRANSLATOR_SIMPLETREE_H
#define TRANSLATOR_SIMPLETREE_H

#include <memory>
#include <vector>
#include "Node.h"

class LinearizationTree {
public:
	LinearizationTree(const std::vector<std::vector<int>> &precedence_matrix);

	void RemoveNode(const std::shared_ptr<Node> &node);

	void RemoveSimpleNodes();

	std::vector<std::shared_ptr<Node>> m_nodes;

	std::vector<std::shared_ptr<Node>>::iterator FindNode(const std::string &node_name,
														  bool canNotFound = false);

	void PrintTree();

private:
	void resetNode(const std::shared_ptr<Node> &node);

	void mergeNodes(const std::vector<std::shared_ptr<Node>> &nodes);
};


#endif //TRANSLATOR_SIMPLETREE_H
