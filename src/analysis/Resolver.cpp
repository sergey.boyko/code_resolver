//
// Created by bso on 18.09.17.
//

#include <algorithm>
#include <memory>
#include "Resolver.h"
#include "../ProgramLog.h"

Resolver::Resolver(const config_parser &parser):
        m_rules(parser.rules),
        m_input_line(parser.input_line),
        m_terminals(parser.terminals){
    m_output_chain.push_back(m_rules[0].rule);
    m_output_chain_iterator = m_output_chain.begin();
    m_inline_iterator = m_input_line.begin();
}

bool Resolver::resolve(std::string &result){
    while (true){
        LOG_D() << "***Next iteration***";
        printChangeHistory();
        printOutputChain();
        printInlineIterator();
        printOutputChainIterator();

        if (!isTerminal(*m_output_chain_iterator)){
            LOG_D() << "Symbol is not terminal => do sprawl";
            //step 1
            sprawl();
            continue;
        }

        if (checkEquality(*m_output_chain_iterator)){
            switch (onEqualityTerminals(result)){
                case 1:{
                    //All ok
                    return true;
                }
                case 2:{
                    //Couldn't find alternative
                    return false;
                }

                case 0:
                default:{
                    //continue
                    break;
                }
            }
        } else{
            if (!loopFindAndReplaceAlternative()){
                return false;
            }
        }
    }
}

void Resolver::sprawl(){
    //find rule of m_output_chain_iterator, push his alternatives in m_output_chain and push
    //rule_info in m_change_history
    auto rule = std::find_if(m_rules.begin(), m_rules.end(), [=](const rule_info &_rule){
        return (*m_output_chain_iterator == _rule.rule) &&
               (_rule.number_alt == 1);
    });
    if (rule == m_rules.end()){
        LOG_E() << "m_output_chain_iterator has no alternatives";
        throw std::runtime_error("m_output_chain_iterator has no alternatives");
    }

    replaceAlternative(*rule);
}

bool Resolver::checkEquality(char symbol){
    LOG_D() << "Check equality \'" << symbol << "\' and \'" << *m_inline_iterator << "\'";
    return symbol == *m_inline_iterator;
}

bool Resolver::isTerminal(char symbol){
    return std::find(m_terminals.begin(), m_terminals.end(), symbol) != m_terminals.end();
}

bool Resolver::loopFindAndReplaceAlternative(){
    LOG_D() << "loopFindAndReplaceAlternative()";
    if (m_change_history.empty()){
        return false;
    }
    //Find alternative from more top rule
    while (!m_change_history.empty()){
        if (isTerminal(m_change_history.top().rule)){
            //step back by terminal
            LOG_D() << "Step back";
            m_change_history.pop();
            --m_output_chain_iterator;
            --m_inline_iterator;
            continue;
        }

        if (swapAlternatives()){
            return true;
        } else{
            stepBack();
        }
    }

    return false;
}

void Resolver::replaceAlternative(const rule_info &rule){
    m_change_history.push(rule);
    if (m_output_chain_iterator == m_output_chain.end()){
        LOG_E() << "m_output_chain_iterator == m_output_chain.end()";
        throw std::runtime_error("m_output_chain_iterator == m_output_chain.end()");
    }
    for (int i = rule.alternative.size() - 1; i >= 0; --i){
        m_output_chain_iterator =
                m_output_chain.insert(m_output_chain_iterator + 1, rule.alternative[i]) - 1;
    }

    m_output_chain_iterator = m_output_chain.erase(m_output_chain_iterator);
}

bool Resolver::checkResult(){
    return m_output_chain_iterator == m_output_chain.end()
           && m_inline_iterator == m_input_line.end();
}

void Resolver::fillResult(std::string &result){
    std::stack<rule_info> cpy(m_change_history);

    std::vector<std::string> res(cpy.size());
    while (!cpy.empty()){
        res[cpy.size() - 1] = cpy.top().rule + std::to_string(cpy.top().number_alt) + ' ';
        cpy.pop();
    }

    std::stringstream ss;
    for (const auto &it : res){
        ss << it;
    }

    result = ss.str();
}

bool Resolver::swapAlternatives(){
    LOG_D() << "swapAlternatives()";
    auto rule = findAlternativeRule();
    if (rule == m_rules.end()){
        return false;
    }

    stepBack();
    replaceAlternative(*rule);

    return true;
}

void Resolver::stepBack(){
    LOG_D() << "stepBack()";
    for (char it : m_change_history.top().alternative){
        if (it != *m_output_chain_iterator){
            throw std::runtime_error("resolver::stepBack() A");
        }

        m_output_chain.erase(m_output_chain_iterator);
    }

    //insert before iterator
    m_output_chain.insert(m_output_chain_iterator, m_change_history.top().rule);
    m_change_history.pop();
}

std::vector<rule_info>::iterator Resolver::findAlternativeRule(){
    return std::find_if(m_rules.begin(),
                        m_rules.end(),
                        [=](const rule_info &_rule){
                            return (_rule.rule == m_change_history.top().rule) &&
                                   (_rule.number_alt > m_change_history.top().number_alt);
                        });
}

void Resolver::printOutputChain(){
    std::stringstream s;
    for (auto it : m_output_chain){
        s << it << ' ';
    }
    LOG_D() << "m_output_chain = " << s.str();
}

void Resolver::printChangeHistory(){
    std::stack<rule_info> cpy(m_change_history);

    std::vector<std::string> res(cpy.size());
    while (!cpy.empty()){
        res[cpy.size() - 1] = cpy.top().rule + std::to_string(cpy.top().number_alt) + ' ';
        cpy.pop();
    }

    std::stringstream ss;
    for (const auto &it : res){
        ss << it;
    }

    LOG_D() << "m_change_history = " << ss.str();
}

void Resolver::printInlineIterator(){
    if (m_inline_iterator != m_input_line.end()){
        LOG_D() << "m_inline_iterator = " << *m_inline_iterator;
    }
}

void Resolver::printOutputChainIterator(){
    if (m_output_chain_iterator != m_output_chain.end()){
        LOG_D() << "m_inline_iterator = " << *m_output_chain_iterator;
    }

}

int Resolver::onEqualityTerminals(std::string &result){
    m_change_history.push(rule_info(*m_output_chain_iterator,
                                    "",
                                    0));
    ++m_output_chain_iterator;
    ++m_inline_iterator;
    if (m_output_chain_iterator == m_output_chain.end()){
        if (checkResult()){
            fillResult(result);
            return 1;
        }
        if (!loopFindAndReplaceAlternative()){
            return 2;
        }
    }

    return 0;
}
