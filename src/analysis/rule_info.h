//
// Created by bso on 11.09.17.
//

#ifndef COMPIL_METH_RULE_INFO_H
#define COMPIL_METH_RULE_INFO_H

#include <string>

class rule_info{
public:
    rule_info(char _rule,
              const std::string &_alternative,
              int _number_alt): rule(_rule),
                                alternative(_alternative),
                                number_alt(_number_alt){

    }

    rule_info(const rule_info &_rule_info) = default;

    char rule;
    std::string alternative;
    int number_alt;
};


#endif //COMPIL_METH_RULE_INFO_H