//
// Created by bso on 18.09.17.
//

#ifndef TRANSLATOR_RESOLVER_H
#define TRANSLATOR_RESOLVER_H


#include <stack>
#include <queue>
#include "rule_info.h"
#include "../config_parser.h"

class Resolver{
public:
    explicit Resolver(const config_parser &parser);

    bool resolve(std::string &result);

private:
    //step 1
    void sprawl();

    bool checkEquality(char symbol);

    bool isTerminal(char symbol);

    bool loopFindAndReplaceAlternative();

    void replaceAlternative(const rule_info &rule);

    bool checkResult();

    void fillResult(std::string &result);

    bool swapAlternatives();

    void stepBack();

    int onEqualityTerminals(std::string &result);

    void printOutputChain();

    void printChangeHistory();

    void printInlineIterator();

    void printOutputChainIterator();

    std::vector<rule_info>::iterator findAlternativeRule();

    std::vector<char>::iterator m_inline_iterator;

    /**
     * history changes
     */
    std::stack<rule_info> m_change_history;
    /**
     * output chain
     */
    std::vector<char> m_output_chain;

    std::vector<char>::iterator m_output_chain_iterator;
    /**
     * rules map with key = rule_info.rule
     */
    std::vector<rule_info> m_rules;
    /**
     * input line
     */
    std::vector<char> m_input_line;

    std::vector<char> m_terminals;
};


#endif //TRANSLATOR_RESOLVER_H
